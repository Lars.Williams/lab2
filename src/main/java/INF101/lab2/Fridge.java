package INF101.lab2;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	
	List<FridgeItem> fridge = new ArrayList<FridgeItem>();

	
	


	
	public int nItemsInFridge() {


		return fridge.size();
	}
	
	
	public int totalSize() {
		return 20;
	}
	
	
	public boolean placeIn(FridgeItem item) {
		if(nItemsInFridge() < totalSize()) {

			fridge.add(item);
			


			return true;
			
		}
		else {return false;}
	}
 
	public void takeOut(FridgeItem item)  {
		

		
		boolean b = fridge.contains(item);
		System.out.println(b);

			
		
		if (fridge.isEmpty()){
			
			throw  new NoSuchElementException();

		}
		
		else {
			fridge.remove(item);
			
		}
	}
		
	
	
	public void emptyFridge() {
		fridge.clear();	
	}
	
	
	
	public List<FridgeItem> removeExpiredFood() {
		
		LocalDate expiredDate = LocalDate.of(2008, 4, 19);
		List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
		List<String> itemNames1 = Arrays.asList("Chicken", "Tofu", "Pizza", "Milk");

			for (var i = 0; i < itemNames1.size(); i++) {
				FridgeItem expiredItem = new FridgeItem(itemNames1.get(i), expiredDate);
				expiredItems.add(expiredItem);
				}

		
		
		for (var i = 0; i < fridge.size();) {

			Object testFood = fridge.get(i);
			
			
			if (fridge.get(i).hasExpired()) {

				
				fridge.remove(testFood);
				
				
			} 
			else {
			i = i+1;
			
			}
		

		}
		
		return expiredItems;

	}
	
	
}
